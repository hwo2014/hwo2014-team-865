import json
import socket
import sys


class VladimirBot(object):
    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        self.PI = 3.14159265359
        self.prev_position = 0
        self.prev_pieceId = 0

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    # original
    # def join(self):
    #     return self.msg("join", {"name": self.name,
    #                              "key": self.key})

    def join(self):
        return self.msg("createRace", {"botId":{"name": self.name, "key": self.key},
                                       "trackName": "france"})

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def on_create_race(self):
        return self.msg("createRace", {"botId":{"name": self.name ,"key": self.key},
                                      "trackName": "usa", "password": "pass0", "carCount": 2})
    def on_join_race(self):
        return self.msg("joinRace", {"botId":{"name": self.name ,"key": self.key},
                                      "trackName": "keimola", "password": "pass0", "carCount": 2})

    # original one
    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print "on join:", data
        print("Joined")
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def on_game_init(self, data):
        self.trackData = data
        # print "data of track:", data['race']['track']['pieces']

    def on_car_positions(self, data):
        data = self.findMyCar(data)

        throttle = self.getThrottle(data)
        # print throttle

        self.throttle(throttle)

    def on_crash(self, data):
        print("Someone crashed")
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def getThrottle(self, data):
        cur_speed = self.getCurrentSpeed(data)
        max_speed = self.getMaximumSpeed(data['angle'], data['piecePosition']['pieceIndex'], cur_speed, data)

        if max_speed > 0:
            if cur_speed < max_speed:
                return 1.0
            else:
                is_next_piece_turn = self.getAngle(data['piecePosition']['pieceIndex'] + 1)
                if self.getAngle(data['piecePosition']['pieceIndex']) and not is_next_piece_turn:
                    end_of_turn = 100 - data['piecePosition']['inPieceDistance']
                    if end_of_turn < 50:
                        return 1.0
                return 0
        else:
            return 1.0

    def getAngle(self, pieceId):
        maximum_pieceId = len(self.trackData['race']['track']['pieces']) - 1

        if pieceId > maximum_pieceId:
            pieceId = 0

        piece = self.trackData['race']['track']['pieces'][pieceId]

        if 'angle' in piece:
            return piece['angle']
        else:
            return 0

    def getRadius(self, pieceId):
        piece = self.trackData['race']['track']['pieces'][pieceId]

        if 'radius' in piece:
            return piece['radius']
        else:
            return 0

    def findMyCar(self, data):
        for item in data:
            if 'red' in item['id']['color'] and 'kersh' in item['id']['name']:
                return item

    def getCurrentSpeed(self, data):
        if data['piecePosition']['pieceIndex'] == self.prev_pieceId:
            car_speed = data['piecePosition']['inPieceDistance'] - self.prev_position
            self.prev_position = data['piecePosition']['inPieceDistance']
        else:
            car_speed = data['piecePosition']['inPieceDistance'] - 0
            self.prev_position = data['piecePosition']['inPieceDistance']
            self.prev_pieceId = data['piecePosition']['pieceIndex']

        return car_speed

    def getMaximumSpeed(self, car_angle, pieceId, cur_speed, data):
        radius = self.getRadius(pieceId)
        angle = self.getAngle(pieceId)
        is_next_piece_turn = self.getAngle(pieceId + 1)
        arc_length = (angle / 360) * 2 * self.PI * radius

        # physics and car values
        ########################
        gravity = 9.8 # m/s
        mass = 1100 # kg
        # coefficient of friction
        u = 0.04


        if not is_next_piece_turn and not self.getAngle(pieceId):
            return 0
        if is_next_piece_turn or self.getAngle(pieceId):
            if is_next_piece_turn:
                radius = self.getRadius(pieceId + 1)
            if self.getAngle(pieceId):
                radius = self.getRadius(pieceId)

            # find normal force(N)
            f_normal = mass * gravity

            # find friction force(N)
            f_friction = u * f_normal

            # find the force of centripetal force(N)
            # f_centripetal = mass * (v^2 / radius)
            f_centripetal = mass / radius

            # return maximum car speed on the turn
            return ((f_friction / f_centripetal) ** (0.5))
        else:
            end_of_line = 100 - data['piecePosition']['inPieceDistance']
            if end_of_line < 50:
                return 1.0
            else:
                return 0



    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'gameInit': self.on_game_init,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = VladimirBot(s, name, key)
        bot.run()
