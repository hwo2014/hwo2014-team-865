import json
import socket
import sys
import math
#TODO something weird going on with angles and corners and speed and shit
class StartBot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        self.track = []
        self.current_throttle = 0
        self.terminal_velocity = 10       #possibly make into a list for all the maps?
        self.lanes = 0
        self.previous_drift_angle = 0
        self.current_lane = 0
        self.previous_position = 0
        self.distance_per_tick = 0
        self.gametick = 0
        self.car_count = 1
        self.car_index = 0
        self.previous_distance_per_tick = 0
        self.previous_speeds = []
        self.mass = 0
        self.drag = 0
        self.friction = 0.02
        self.g = 9.81
        self.signaled_to_switch = False


    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})
    def on_create_race(self):
        return self.msg("createRace", {"botId":{"name": self.name ,"key": self.key},
                                      "trackName": "germany", "password": "pass0", "carCount": 1})
    def on_join_race(self):
        return self.msg("joinRace", {"botId":{"name": self.name ,"key": self.key},
                                      "trackName": "germany", "password": "pass0", "carCount": 1})

    def on_join_faf(self):
        return self.msg("joinRace", {"botId":{"name": self.name ,"key": self.key},
                                       "carCount": 3})

    def throttle(self, throttle):
        self.current_throttle = throttle
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        #self.on_create_race()
        #self.on_join_race()
        #self.on_join_faf()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.throttle(1.0)

    def left_or_right_turn(self, angle):
        #For left turns, the angle is a negative value
        if angle > 0:
            return "Right"
        if angle < 0:
            return "Left"

    def left_or_right_lane(self,turn):
        if turn == "Right":
            return 1
        else:
            return 0


    def switch_lanes(self,direction):
        self.msg("switchLane", direction)

    def corner_apex(self,curlen):
        pass

    def get_drag(self):
        self.drag = (self.previous_speeds[2] - (self.previous_speeds[3] - self.previous_speeds[2])) / (pow(self.previous_speeds[2],2) * self.current_throttle)

    def get_mass(self):
        self.mass = 1.0 / (math.log((self.previous_speeds[4] - (self.current_throttle / self.drag)) / (self.previous_speeds[3] - (self.current_throttle / self.drag))) / -(self.current_throttle))

    def get_terminal_velocity(self):
        if self.drag != 0:
            self.terminal_velocity = self.current_throttle / self.drag

    def overdrift(self, angle):
        angle = abs(angle)
        rate_of_change_of_angle = angle - self.previous_drift_angle
        if rate_of_change_of_angle >= 2 or (rate_of_change_of_angle * 4) + angle >= 40: # rate_of_change >=4 + angle >=45
            return True

    def get_accelaration(self):
        return round(self.distance_per_tick - self.previous_distance_per_tick,2)

    def get_angle(self):
        return self.track["pieces"][0].get('angle')

    def get_length(self,piece=0):
        if self.track["pieces"][piece].get('length'):
            return self.track["pieces"][piece].get('length')
        else:
            return math.radians(abs(self.track["pieces"][piece].get('angle'))) * self.get_radius(piece) #which is probably how far the car flag is?
        #BUG: self.track["pieces"][0].get('radius') instead of self.get_radius()


    def should_we_switch(self):
        #. A positive value tells that the lanes is to the right from the center
        #  line while a negative value indicates a lane to the left from the center.
        for i in range(1, len(self.track["pieces"])):
            if self.track["pieces"][i].get('angle'):
                angle = self.track["pieces"][i].get('angle')
                if self.current_lane == self.lanes and self.switch_lanes(self.left_or_right_turn(angle)) == "Right": #we are rightmost
                    return False
                if self.current_lane == 0 and self.switch_lanes(self.left_or_right_turn(angle)) == "Left":
                    return False
                print "Switched to", self.left_or_right_turn(angle)
                self.current_lane = self.left_or_right_lane(self.left_or_right_turn(angle))
                return self.left_or_right_turn(angle)


    def debug_info(self,data):
        print "Track piece" , self.track["pieces"][0]
        #print "Length piece", self.get_length()
        print "current position", data[self.car_index]["piecePosition"]["inPieceDistance"]
        print "Distance per tick" , self.distance_per_tick
        #print "Angle", self.track["pieces"][0].get('angle')
        #print "Radius", self.get_radius()
        #print "Maximum Speed" , self.get_maximum_speed()
        print "Distance to Stop" , self.get_distance_to_stop()
        #print "Ticks", self.get_ticks_to_stop()
        print "throttle", self.current_throttle
        print "drift angle", self.previous_drift_angle
        #print data[0]["angle"]
        #print "accelaration", self.get_accelaration()
        print "radius", self.get_radius()
        print "Current lane" ,self.current_lane
        print "--------------------"

    def get_radius(self,track_piece=0):
        if self.track["pieces"][track_piece].get('radius'):
            angle = self.track["pieces"][track_piece].get('angle')
            lane_radius = 0
            if angle > 0 and self.current_lane == 0: # right corner , external
                lane_radius = self.track["lanes"][self.current_lane]["distanceFromCenter"] * -1# can index be different than its position?!!??!
            elif angle > 0 and self.current_lane == self.lanes: #right corner,internal
                lane_radius = self.track["lanes"][self.current_lane]["distanceFromCenter"]
            elif angle < 0 and self.current_lane == 0: #left corner , internal
                lane_radius = self.track["lanes"][self.current_lane]["distanceFromCenter"]
            elif angle <0 and self.current_lane == self.lanes: # left corner ,external
                lane_radius = self.track["lanes"][self.current_lane]["distanceFromCenter"] * -1
            return self.track["pieces"][track_piece].get('radius') + lane_radius
        return None

    def speed_per_tick(self, data):
        current_position = data[self.car_index]["piecePosition"]["inPieceDistance"]
        self.previous_distance_per_tick = self.distance_per_tick

        if current_position - self.previous_position < 0:
            self.distance_per_tick = self.get_length() - (current_position + self.previous_position)
        else:
            self.distance_per_tick = current_position - self.previous_position
        self.previous_position = current_position
        return self.distance_per_tick

    def bootstrap(self,data):
        self.get_drag()
        print self.drag
        self.get_mass()
        print self.mass
        self.get_friction()
        self.get_g()
        self.get_terminal_velocity()
        self.current_lane = int(data[self.car_index]["piecePosition"]["lane"]["startLaneIndex"])

    def get_centripetal(self,piece=0):
        return (self.mass * pow(self.distance_per_tick,2)) / self.get_radius(piece)

    def get_friction(self):
        self.friction = 1.0 / self.mass

    def get_g(self):
        self.g = self.friction * self.mass * 10 # just for good measure

    def on_car_positions(self, data,ticks=0):
        self.gametick = ticks
        print self.gametick
         #bug with switchlanes
        self.speed_per_tick(data)
        self.debug_info(data)
        self.previous_speeds.append(self.distance_per_tick)

        if ticks == 5:
            self.bootstrap(data)




        if self.is_the_piece_finished(self.get_length(), data[self.car_index]["piecePosition"]["inPieceDistance"]):
            self.track["pieces"].append(self.track["pieces"].pop(0))
            if self.should_we_switch():
                self.switch_lanes(self.should_we_switch())

        '''if self.overdrift(data[self.car_index]["angle"]):
            print "=-=-=-=-=-=-OVERDRIFT=-=-=-=-=-=-"
            self.previous_drift_angle = abs(data[self.car_index]["angle"])
            return self.throttle(0)
        '''
        #instead of brake try to see what happens if you reduce to the maximum velocity

        self.previous_drift_angle = abs(data[self.car_index]["angle"])

        if self.should_we_brake(self.get_length(), data[self.car_index]["piecePosition"]["inPieceDistance"], self.get_radius(1)):
            print "BRAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAKE"
            return self.throttle(0)

        return self.get_velocity()

    def is_the_piece_finished(self, piecelen, curlen):
        if curlen > 0 and piecelen <= curlen + self.distance_per_tick:
            return True

    def get_distance_to_stop(self):
        return pow(self.distance_per_tick, 2) / (2 * self.g * self.friction)

    def get_ticks_to_stop(self):
        if self.distance_per_tick !=0:
            return (2 * self.get_distance_to_stop()) / self.distance_per_tick

    def on_get_track(self,data):
        print "track data", data
        self.track = data["race"]["track"]
        i = 0
        for car in data["race"]["cars"]:
            if car["id"]["color"] == self.car_color and car["id"]["name"] == self.car_name:
                self.car_index = i
                print self.car_index
                break
            i+=1

    def should_we_brake(self, piecelen, curlen, radius):
        if self.distance_per_tick < 0: #BUUUG
            return False
        if piecelen is None:
            return False
        if curlen + self.distance_per_tick + (math.ceil(self.get_distance_to_stop() * 0.3333)) >= piecelen and radius: # distance_to_stop() / 2
            if self.get_maximum_speed(1) <= self.distance_per_tick:
                return True

    def get_maximum_speed(self,track_piece=0):
        if self.get_radius(track_piece):
            return round(math.sqrt(self.get_radius(track_piece)*self.friction *self.g ), 2)
        return self.terminal_velocity - 1 #It's over 9000

    def get_velocity(self,piece=0): #0.027 with angle * 4 >= 45
        if self.get_radius(piece) is None:
            return self.throttle(1.0)
        else:
            v = round(math.log10(math.sqrt(self.get_radius(piece)*self.friction*self.g)), 2)
            if v > 1:
                v = self.get_terminal_velocity() / 10
            return self.throttle(v)

    def on_crash(self, data):
        print("=*=*=*=*=*==*=*==*=*=*=*=*==*=*=Someone crashed=*=*=*=*=*=*==*=*=*=*=*=*=*=*=*==*=*=*")
        print("=*=*=*=*=*==*=*==*=*=*=*=*==*=*=Someone crashed=*=*=*=*=*=*==*=*=*=*=*=*=*=*=*==*=*=*")
        print("=*=*=*=*=*==*=*==*=*=*=*=*==*=*=Someone crashed=*=*=*=*=*=*==*=*=*=*=*=*=*=*=*==*=*=*")
        print("=*=*=*=*=*==*=*==*=*=*=*=*==*=*=Someone crashed=*=*=*=*=*=*==*=*=*=*=*=*=*=*=*==*=*=*")
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        #print self.previous_speeds
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def my_car(self, data):
        self.car_name = data["name"]
        self.car_color = data["color"]
        print self.car_name
        print self.car_color

    '''def start_sequence(self):
        self.join()
        read.yourCar
        read.gameInit
        read.gameStart
        read.carPositions
        self.throttle(0.1)
    '''

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            #'createRace':self.on_create_race,
            'yourCar':self.my_car,
            'gameInit':self.on_get_track,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                if msg_type == 'carPositions' and 'gameTick' in msg:

                    self.on_car_positions(data, msg['gameTick'])
                    #msg_map[msg_type](data, msg['gameTick'])
                else:
                    msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = StartBot(s, name, key)
        bot.run()
